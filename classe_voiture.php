<?php
class Voiture {
    // Attributs
    private $marque;
    private $modèle;
    private $immatriculation;

    // Constructeur
    public function __construct($marque, $modèle, $immatriculation) {
        $this->marque = substr($marque, 0, 25); // Limite à 25 caractères
        $this->modèle = substr($modèle, 0, 10); // Limite à 10 caractères
        $this->immatriculation = substr($immatriculation, 0, 7); // Limite à 7 caractères
    }

    // Méthodes pour accéder en lecture seule
    public function getMarque() {
        return $this->marque;
    }

    public function getModèle() {
        return $this->modèle;
    }

    public function getImmatriculation() {
        return $this->immatriculation;
    }
}
?>
